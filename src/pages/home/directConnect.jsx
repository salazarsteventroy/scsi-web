import { useRef, useEffect } from "react";
import BlcSec from "../../components/Blc-sec";
import CallToAction from "../../components/Call-to-action";
import Clients1 from "../../components/Clients1";
import Footer from "../../components/Footer";
import IntroWithSlider2 from "../../components/Intro-with-slider2";
import Navbar from "../../components/Navbar";
import PortfolioCustomColumn from "../../components/Portfolio-custom-column";
import Services2 from "../../components/Services2";
import SkillsCircle from "../../components/Skills-circle";
import VideoWithTestimonials from "../../components/Video-with-testimonials";
import DarkTheme from "../../layouts/Dark";
import DirectConnectIntro from "../../components/Intro-txt2/directConnectIntro";
import IntroTxt2 from "../../components/Intro-txt2";
import DcServices from "../../components/Services2/DcServices";
import AboutUs1 from "../../components/About-us1";
import WhyChooseDC from "../../components/About-us1/WhyChooseDC";
import TheDirectConnectNetwork from "../../components/DirectConnectNetwork";
import LightTheme from "../../layouts/Light";
import Works2Slider from "../../components/Works2-slider";

const DirectConnect = () => {
    const navbarRef = useRef(null);
    const logoRef = useRef(null);
  
    useEffect(() => {
      var navbar = navbarRef.current;
  
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
      window.addEventListener("scroll", () => {
        if (window.pageYOffset > 300) {
          navbar.classList.add("nav-scroll");
        } else {
          navbar.classList.remove("nav-scroll");
        }
      });
  
      window.addEventListener("load", () => {
        document.body.classList.add('dark2');
        var slidHeight = document.querySelector('.fixed-slider').getBoundingClientRect().height;
        document.querySelector('.main-content').style.setProperty('margin-top', slidHeight + 'px');
      })
  
      // return () => {
      //   document.body.classList.remove('dark2');
      // }
    }, [navbarRef]);
  
  return (
    <>
    <LightTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      <DirectConnectIntro />
      <div className="main-content">
      <BlcSec />
      
      <WhyChooseDC />
      <Works2Slider subBG />
      {/* <DcServices />     */}
      <TheDirectConnectNetwork/>
      <CallToAction />
      <Footer />
    </div>
    </LightTheme>
    </>
  );
};

export default DirectConnect;
