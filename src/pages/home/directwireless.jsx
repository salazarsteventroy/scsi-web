import { useRef, useEffect } from "react";
//= ================== Layout ==================//
import DarkTheme from "../../layouts/Dark";
//= ================== Components ==================//
import Navbar from "../../components/Navbar";
import Intro from "../../components/Intro-txt2";
import Features from "../../components/Features";
import ServicesTop from "../../components/Services5";
import Works from "../../components/Works3-slider";
import Skills from "../../components/Skills-circle2";
import ServicesBottom from "../../components/Services6";
import Testimonials from "../../components/Testimonials2";
import Clients from "../../components/Clients1";
import Blogs from "../../components/Blogs-three-column1";
import CallToAction from "../../components/Call-to-action";
import Footer from "../../components/Footer";
import AboutusDirectWireless from "../../components/About-us2/aboutusDirectWireless";
import IntroWithSlider3 from "../../components/Intro-with-slider3";
import DirectWirelessSlider from "../../components/Intro-with-slider3/DirectWirelessSlider";
import Works1Slider from "../../components/Works1-slider";
import DWManagementSlider from "../../components/Works1-slider/DwManagementSlider";
import VideoDW from "../../components/Numbers-with-video/videoDw";
import DirectWirelessDetails from "../../components/Testimonials2/directwirelessDetails";
import LightTheme from "../../layouts/Light";

const Homepage6 = () => {
  const navbarRef = useRef(null);
  const logoRef = useRef(null);

  useEffect(() => {
    var navbar = navbarRef.current;

    if (window.pageYOffset > 300) {
      navbar.classList.add("nav-scroll");
    } else {
      navbar.classList.remove("nav-scroll");
    }
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        navbar.classList.add("nav-scroll");
      } else {
        navbar.classList.remove("nav-scroll");
      }
    });

    window.addEventListener("load", () => {
      document.body.classList.add("dark2");
      var slidHeight = document
        .querySelector(".fixed-slider")
        .getBoundingClientRect().height;
      document
        .querySelector(".main-content")
        .style.setProperty("margin-top", slidHeight + "px");
    });

    // return () => {
    //   document.body.classList.remove('dark2');
    // }
  }, [navbarRef]);

  return (
    <LightTheme>
      <Navbar nr={navbarRef} lr={logoRef} />
      <Intro />

      <div className="main-content">
      
        <AboutusDirectWireless skillsTheme="light" />
        <DirectWirelessSlider />
        
        {/* <ServicesTop /> */}
        {/* <Works /> */}
        <VideoDW/>
        {/* <DWManagementSlider /> */}
        <DirectWirelessDetails/>
    
        {/* <Skills theme="dark" />
        <ServicesBottom /> */}
        {/* <Testimonials />
        <Clients theme="dark" />
        <Blogs newHome subBG />
        <CallToAction theme="dark" /> */}
        <Footer noSubBG />
      </div>
    </LightTheme>
  );
};

export default Homepage6;
