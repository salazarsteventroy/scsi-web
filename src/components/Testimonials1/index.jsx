/* eslint-disable @next/next/no-img-element */
import React from "react";
import Split from "../Split";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import removeOverlay from "../../common/removeOverlay";
import parallaxie from "../../common/parallaxie";

class Testimonials1 extends React.Component {
  constructor(props) {
    super(props);
  }

  renderArrows = () => {
    return (
      <div className="arrows">
        <div
          onClick={() => this.slider.slickNext()}
          className="next cursor-pointer"
        >
          <span className="pe-7s-angle-right"></span>
        </div>
        <div
          onClick={() => this.slider.slickPrev()}
          className="prev cursor-pointer"
        >
          <span className="pe-7s-angle-left"></span>
        </div>
      </div>
    );
  };

  componentDidMount() {
    removeOverlay();
    parallaxie(".testimonials.bg-img.parallaxie");
  }

  render() {
    return (
      <section
        className={`testimonials section-padding ${
          this.props.subBgLftstl ? "sub-bg lftstl" : ""
        } ${this.props.withBG ? "bg-img" : ""} ${
          this.props.parallaxie ? "parallaxie" : ""
        } ${!this.props.overlay ? "noOverlay" : ""}`}
        style={{
          backgroundImage: `${
            this.props.withBG && !this.props.imgSrc
              ? "url(/img/slid/3.jpg)"
              : this.props.imgSrc
              ? `url(${this.props.imgSrc})`
              : "none"
          }`,
        }}
        data-overlay-dark={`${this.props.overlay ? "9" : "0"}`}
      >
        <div className="container position-re">
          <div className="sec-head custom-font text-center">
            <h6 className="wow fadeIn" data-wow-delay=".5s">
              Testimonials.
            </h6>
            <Split>
              <h3 className="wow words chars splitting" data-splitting>
                WHAT OUR CUSTOMERS SAY
              </h3>
            </Split>
            <span className="tbg">Testimonials</span>
          </div>
          <div
            className="row justify-content-center wow fadeInUp"
            data-wow-delay=".5s"
          >
            <div className="col-lg-9">
              <Slider
                className="slic-item"
                {...{
                  ref: (c) => (this.slider = c),
                  dots: true,
                  infinite: true,
                  arrows: true,
                  autoplay: true,
                  rows: 1,
                  slidesToScroll: 1,
                  slidesToShow: 1,
                }}
              >
                <div className="item">
                  <p>
                    We now utilise the services provided by SCSI to not only
                    monitor sites with both GSM and GPRS solutions but with a
                    full redundancy back up communications path for our PSTN
                    lines. We strongly believe that our two companies have
                    developed a business relationship that is
                  </p>
                  <div className="info">
                    <div className="cont">
                      <div className="author">
                        {this.props.subBgLftstl ? (
                          <>
                            <div className="lxleft">
                              {/* <div className="img">
                                <img src="/img/clients/1.jpg" alt="" />
                              </div> */}
                            </div>
                            <div className="fxright">
                              <h6 className="author-name custom-font">
                                Gary Palmer
                              </h6>
                              <span className="author-details">
                                Director - Alarmnet
                              </span>
                            </div>
                          </>
                        ) : (
                          <>
                            {/* <div className="img">
                              <img src="/img/clients/1.jpg" alt="" />
                            </div> */}
                            <h6 className="author-name custom-font">
                              Gary Palmer
                            </h6>
                            <span className="author-details">
                              Director - Alarmnet
                            </span>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <p>
                    With regards to Direct Wireless, in our opinion it is truly going to be the ultimate in high level Alarm Monitoring. Having tested the system on the Sunshine Coast where GSM is known for its unreliability, the Direct Wireless System hasn&apos;t failed once and we are extremely excited about the potential for this product and the benefits to both ourselves and the end user/customer. It is a product that we wouldn&apos;t hesitate to recommend.
                  </p>
                  <div className="info">
                    <div className="cont">
                      <div className="author">
                        {this.props.subBgLftstl ? (
                          <>
                            <div className="lxleft">
                              {/* <div className="img">
                                <img src="/img/clients/1.jpg" alt="" />
                              </div> */}
                            </div>
                            <div className="fxright">
                              <h6 className="author-name custom-font">
                                Maks Maksan
                              </h6>
                              <span className="author-details">
                                Managing Director - Naksam
                              </span>
                            </div>
                          </>
                        ) : (
                          <>
                            {/* <div className="img">
                              <img src="/img/clients/1.jpg" alt="" />
                            </div> */}
                            <h6 className="author-name custom-font">
                              Maks Maksan
                            </h6>
                            <span className="author-details">
                              Managing Director - Naksam
                            </span>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                {/* <div className="item">
                  <p>
                    I would highly recommend Avo Digital. I worked with the team
                    on an animation for our U&apos;2018 Click &amp; Collect U&apos;2019
                    service. This team is different from any other agency I have
                    worked with in the past.
                  </p>
                  <div className="info">
                    <div className="cont">
                      <div className="author">
                        {this.props.subBgLftstl ? (
                          <>
                            <div className="lxleft">
                              <div className="img">
                                <img src="/img/clients/1.jpg" alt="" />
                              </div>
                            </div>
                            <div className="fxright">
                              <h6 className="author-name custom-font">
                                Alex Regelman
                              </h6>
                              <span className="author-details">
                                Co-founder, Colabrio
                              </span>
                            </div>
                          </>
                        ) : (
                          <>
                            <div className="img">
                              <img src="/img/clients/1.jpg" alt="" />
                            </div>
                            <h6 className="author-name custom-font">
                              Alex Regelman
                            </h6>
                            <span className="author-details">
                              Co-founder, Colabrio
                            </span>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div> */}
              </Slider>
            </div>
          </div>
          {this.renderArrows()}
        </div>
      </section>
    );
  }
}

export default Testimonials1;
