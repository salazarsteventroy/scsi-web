/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import ModalVideo from "react-modal-video";
import "react-modal-video/css/modal-video.css";
import Split from "../Split";

const VideoDW = ({ theme = "dark" }) => {
  const [isOpen, setOpen] = useState(false);

  return (
    <section className="block-sec">
      <div
        className="background bg-img section-padding pb-0"
        style={{ 
          backgroundImage: `url(/img/pattern${theme === 'light' ? '-light':''}.png)`,
          marginBottom: "200px"
        }}
      >
        {typeof window !== "undefined" && (
          <ModalVideo
            channel="custom"
            autoplay
            isOpen={isOpen}
            url="/img/dwvideo.mp4"
            onClose={() => setOpen(false)}
          />
        )}

        <div className="showreel">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-9">
                <div className="video-box">
                  <div className="tit-text">
                    <Split>
                      <h3 className="wow words chars splitting" data-splitting>
                        DIRECT WIRELESS
                      </h3>
                    </Split>
                  </div>
                  <div className="wow imago">
                    <div className="img">
                      <img src="/img/DWBGIMG.png" alt="" />
                    </div>
                    <div className="vid-icon">
                      <a
                        href="#"
                        className="vid"
                        onClick={(e) => {
                          e.preventDefault();
                          setOpen(true);
                        }}
                      >
                        <div className="vid-butn">
                          <span className="icon">
                            <i className="fas fa-play"></i>
                          </span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default VideoDW;
