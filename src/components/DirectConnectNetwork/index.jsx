import React from 'react'

const TheDirectConnectNetwork = () => {
  return (
    <div
        style={{
            display:'flex',
       
            alignItems:"center",
            justifyContent:"center",
            flexDirection:"column",
            borderRadius:"20px",
            border: "3px solid #1A272F",
            backgroundImage: "linear-gradient(150deg, #121517, #1A272F)",
            paddingTop:"15px",
            paddingBottom:"30px",
            marginLeft:"120px",
            marginRight:"120px"
        }}
    >
      <div className='theDcNetworkContainer'>
            <p className='theDcNetworkTitle'
                style={{
                    textAlign:"center"
                }}
            >
            THE DIRECT CONNECT NETWORK
            </p>
            <div className='theDcImage'>
              <img src="/img/slid/dcnetwork.png" alt="" />
            
                
            </div>
      </div>
    </div>
  )
}

export default TheDirectConnectNetwork
