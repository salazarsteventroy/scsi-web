import React from "react";

const AboutHeader = () => {
  return (
    <header
      className="pages-header bg-img valign parallaxie"
      style={{
        backgroundImage: "url(/img/slid/vidbg3.png)",
        backgroundPosition: "center top", // Center horizontally, align top vertically
        paddingTop: "50px", // Adjust this value to control how much of the top to cut off
      }}
      data-overlay-dark="1"
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="cont text-center">
              <h1>About Us</h1>
              <div className="path">
                <a href="#0">Home</a>
                <span>/</span>
                <a href="#0" className="active">
                  About Us
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default AboutHeader;
