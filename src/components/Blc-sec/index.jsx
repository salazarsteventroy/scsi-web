import React from "react";
import Link from "next/link";
import Split from "../Split";

const BlcSec = () => {
  return (
    <section className="blc-sec section-padding pb-0">
      <div className="container"
        style={{
          paddingBottom:"30px"
        }}
      >
        <div className="row">
          <div className="col-lg-7">
            <div className="intro md-mb30">
              <div className="sub-title">
                <h6>Know Us Better</h6>
                <span></span>
                <span></span>
                <span></span>
              </div>
              <Split>
                <h2
                  className="extra-title wow words chars splitting"
                  data-splitting
                >
                  WHAT IS DIRECTCONNECT?
                </h2>

                <p>
                  DirectConnect is a secure and reliable 4G Fixed IP Sim service
                  that connects field devices such as CCTV, Access Control, BMS,
                  Medical or any IP device via a VPN connection.
                  <br />
                  <br />
                  DirectConnect is deployed through a standard 4G router that
                  connects to SCSI&apos;s private and secure network. This enables
                  the 4G router to have a fixed IP address at all times,
                  removing the inconvenience of having to visit the site to
                  reconfigure hardware should the network allocate another IP
                  address to the device as is typically the case. Users can
                  connect to their sites via the web, Software GUI, Android or
                  IOS devices once they connect to their VPN.
                </p>
              </Split>
            </div>
          </div>
          <div className="col-lg-5 valign">
            <div className="full-width">
              <Split>
                <p className="wow txt words chars splitting" data-splitting>
                  <br />
                  <br />
                  Central Monitoring Stations can easily connect to remote CCTV
                  equipment to carry out Video Verification on alarms as well as
                  offering Virtual Guard Tours to increase revenue, reduce false
                  callout fees and improve customer service.
                  <br />
                  <br />
                </p>
              </Split>
              <Split>
                <Link href="/about/about-dark">
                  <a
                    className="simple-btn custom-font mt-20 wow words chars splitting"
                    data-splitting
                  >
                    <span>
                      {" "}
                      Read the Security Electronics article about DirectConnect.
                    </span>
                  </a>
                </Link>
              </Split>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default BlcSec;
