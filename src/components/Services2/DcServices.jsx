import React from "react";
import Link from "next/link";

const DcServices = () => {
  return (
    <section className="services section-padding"
      style={{
        backgroundColor:"#2f323b"
      }}
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-global"></span>
              <h6>
                4G Connectivity <br /> with 3G failover
              </h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-config"></span>
              <h6>
                Supported in all major <br /> NVR/DVR solutions
              </h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-safe"></span>
              <h6>
                Secure and Robust <br /> Private SCSI network
              </h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-world"></span>
              <h6>VPN Connection</h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-help2"></span>
              <h6>
                iPhone & Android <br /> Support
              </h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-monitor"></span>
              <h6>
                PC and Mac O/S <br /> compatible
              </h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-network"></span>
              <h6>
                Access all your devices from your own
                <br />
                dedicated web portal
              </h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-add-user"></span>
              <h6>Unlimited client profiles</h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-shopbag"></span>
              <h6>Flexible data plans</h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-leaf"></span>
              <h6>Environmentally friendly</h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-bicycle"></span>
              <h6>
                Reduce carbon footprint by
                <br /> bringing devices to you
              </h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="mas-item">
              <span className="icon pe-7s-cloud"></span>
              <h6>Turnkey 4G solution</h6>
              <div className="text-right">
                <Link href="/about/about-dark">
                  <a className="more">
                    <i className="fas fa-chevron-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default DcServices;
