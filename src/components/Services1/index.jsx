import React from "react";
import Split from "../Split";
import Link from "next/link";

const Services1 = () => {
  return (
    <section className="services">
      <div className="container">
        <div className="sec-head custom-font text-center">
          <h6 className="wow fadeIn" data-wow-delay=".5s">
            SCSI
          </h6>
          <Split>
            <h3 className="wow words chars splitting" data-splitting>
              Secure Wireless Technology
            </h3>
          </Split>
          <span className="tbg">Services</span>
        </div>
        <div className="row">
          <div
            className="col-lg-3 col-md-6 item-box bg-img wow fadeInLeft"
            data-wow-delay=".3s"
            style={{ backgroundImage: "url(/img/bg1.jpg)" }}
          >
            <h4 className="custom-font">
            Find out more about the secure wireless technologies that SCSI offer for alarm transmission and monitoring. 
            </h4>
            {/* <Link href="/about/about-dark">
              <a className="btn-curve btn-bord btn-lit mt-40">
                <span>See All Services</span>
              </a>
            </Link> */}
          </div>
          <div
            className="col-lg-3 col-md-6 item-box wow fadeInLeft"
            data-wow-delay=".5s"
          >
            {/* <span className="icon"
                style={{ backgroundImage: "url(/img/directwireless.png)" }}
            ></span> */}
          <img src="/img/directwireless.png">
          </img>
            <h6>SECURE ALARM NETWORK</h6>
            <p>
            Australia’s only dedicated private alarm transmission network providing a world of hassle-free alarm monitoring.
            </p>
          </div>
          <div
            className="col-lg-3 col-md-6 item-box wow fadeInLeft"
            data-wow-delay=".7s"
          >
            {/* <span className="icon pe-7s-phone"></span> */}
            <img src="/img/directconnect.png">
          </img>
            <h6>
            4G FIXED IP SIM
            </h6>
            <p>Secure VPN service using our dedicated private transmission network.</p>
          </div>
          <div
            className="col-lg-3 col-md-6 item-box wow fadeInLeft"
            data-wow-delay=".9s"
          >
        
            <img src="/">
          </img>
            <h6>
           PLACE HOLDER
            </h6>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sit tempora, rerum sunt, sequi labore inventore molestias ullam vero ducimus ad distinctio reprehenderit repudiandae pariatur</p>
          </div>
        </div>
      </div>
      <div className="half-bg bottom"></div>
    </section>
  );
};

export default Services1;
