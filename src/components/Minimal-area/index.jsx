/* eslint-disable @next/next/no-img-element */
import React from "react";
import featuresEffect from "../../common/featuresEffect";
import { thumparallaxDown } from "../../common/thumparallax";
import Split from "../Split";

const MinimalArea = () => {
  React.useEffect(() => {
    featuresEffect();
    setTimeout(() => {
      thumparallaxDown();
    }, 1000);
  }, []);
  return (
    <section className="min-area sub-bg">
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="img">
              <img
                className="thumparallax-down"
                src="/img/SCSI.png"
                alt=""
              />
            </div>
          </div>
          <div className="col-lg-6 valign">
            <div className="content">
              <Split>
                <h4
                  className="wow custom-font words chars splitting"
                  data-splitting
                >
                  SCSI
                </h4>
              </Split>

              <Split>
                <p className="wow txt words chars splitting" data-splitting>
                VISION
                </p>
              </Split>
              <ul className="feat">
                <li className="wow fadeInUp" data-wow-delay=".2s">
                  <h6>
                    <span>1</span> The perfect wireless monitoring solution:
                  </h6>
                  <p>
                 ... a highly secure communications device capable of transmitting alarm signals in the event of line failure tha could replace Securitel and direct line comms in high end applications
                  </p>

                  <br/>

              <p>  <strong>Steve Acott</strong>, SCSI Founder </p>  
                </li>
                <li className="wow fadeInUp" data-wow-delay=".4s">
                  <h6>
                    <span>2</span> Looking forward to a connected world
                  </h6>
                  <p>
                    To be at the forefront of wireless security and safety commmunications, delivering innovative engineered solutions on a global platform, with our network partners
                  </p>
                  <br/>

<p>  <strong>Dale Acott</strong>, SCSI Managing Director </p>  
                </li>
                {/* <li className="wow fadeInUp" data-wow-delay=".6s">
                  <h6>
                    <span>3</span> Why Us?
                  </h6>
                  <p>
                    luctus massa ipsum at tempus eleifend congue lectus bibendum
                  </p>
                </li> */}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default MinimalArea;
