/* eslint-disable @next/next/no-img-element */
import React from "react";
import Link from "next/link";
import Split from "../Split";
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

const AboutusDirectWireless = ({ skillsTheme }) => {
  const cpStyle = {
    path: {
      stroke: "#75dab4",
    },
    trail: {
      stroke: skillsTheme
        ? skillsTheme == "dark"
          ? "#0f1218"
          : "#e5e5e5"
        : "",
    },
    text: {
      fill: skillsTheme ? (skillsTheme == "dark" ? "#ffffff" : "#4e4e4e") : "",
      fontSize: "16px",
    },
  };
  return (
    <section className="about section-padding">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 valign">
            <div className="content">
              <div className="sub-title">
                <h6>Direct Wireless</h6>
                <span></span>
                <span></span>
                <span></span>
              </div>
              <Split>
                <h3
                  className="co-tit custom-font wow words chars splitting"
                  data-splitting
                >
                  Why Choose DirectWireless?
                </h3>
              </Split>
              <p className="wow fadeInUp" data-wow-delay=".4s">
                DirectWireless delivers high grade wireless alarm transmission
                to over 95% of the Australian population and is ideal for high
                risk sites, as well as a ready option for most new or existing
                alarm systems that rely on traditional PSTN phone lines for
                communications. DirectWireless does not have any of the inherent
                pitfalls that the outdated PSTN network has, such as the very
                high risk of phone line sabotage prior to Burglary. And don&apos;t
                worry about the rising cost of special dedicated or supervised
                PSTN landlines for your security system. DirectWireless replaces
                this outdated expensive approach, and it provides a secure
                wireless IP alarm transmission system available with a running
                cost comparable to most PSTN dialler alarm systems.
              </p>
              <p className="mt-10 wow fadeInUp" data-wow-delay=".4s">
                By connecting your alarms to one of the many authorized
                DirectWireless central monitoring stations, you can turn your
                investment into a far more effective security system.
                DirectWireless is designed to meet AS2201.5-1992 Class 2, 3 and
                4 alarm transmission systems, and interfaces to any standard
                alarm control panel, the product is perfect for any installation
                and provides a high level of wireless security
              </p>
              {/* <Split>
                <Link href="/about/about-dark">
                  <a
                    className="words chars splitting simple-btn custom-font mt-30 wow"
                    data-splitting
                  >
                    <span>Know More</span>
                  </a>
                </Link>
              </Split> */}
            </div>
          </div>
          <div className="col-lg-6">
            <div className="blc-img">
              <div className="bimg wow imago">
                <img src="/img/intro/bg2.jpg" alt="" />
              </div>
              <div className="skills-circle wow fadeInUp" data-wow-delay=".8">
                <div className="item">
                  <div className="skill">
                    <CircularProgressbar
                      value={90}
                      className="custom-font"
                      strokeWidth={2}
                      text={`${95}%`}
                      styles={cpStyle}
                    />
                  </div>
                  <div className="cont">
                    <span>Australian</span>
                    <h6> Network Coverage</h6>
                  </div>
                </div>
                <div className="item">
                  <div className="skill">
                    <CircularProgressbar
                      value={55}
                      strokeWidth={2}
                      className="custom-font"
                      text={`${50}+`}
                      styles={cpStyle}
                    />
                  </div>
                  <div className="cont">
                    <span>Over 50</span>
                    <h6>Monitoring Station</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AboutusDirectWireless;
