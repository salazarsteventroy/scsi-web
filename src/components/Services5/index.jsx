/* eslint-disable @next/next/no-img-element */
import { useState, useEffect } from 'react'
import ModalVideo from "react-modal-video";
import "react-modal-video/css/modal-video.css";

const Services5 = () => {
  const [isOpen, setOpen] = useState(false);

  useEffect(() => {
    console.clear();
  }, []);

  return (
    <section className="simpl-intro section-padding">
      <div className="container">
        <div className="row">
          <div className="col-lg-5 offset-lg-1 md-mb50">
            <div className="img">
              <img src="/img/intro/WAwatch.png" alt="" />
              {/* <div className="vid-icon">
                <a className="vid" href="https://vimeo.com/127203262" onClick={(e) => {e.preventDefault(); setOpen(true);}}>
                  <div className="vid-butn">
                    <span className="icon">
                      <i className="fas fa-play"></i>
                    </span>
                  </div>
                </a>
              </div> */}
            </div>
          </div>
          <div className="col-lg-5 offset-lg-1 valign">
            <div className="cont">
              <div className="sub-title">
                <h6>SCSI</h6>
                <span></span>
                <span></span>
                <span></span>
              </div>
              <h3 className="mb-30 fw-700">What is <br />  Watch Armour?.</h3>
              <p>The Watch Armour is a wearable technology for your safety. It is designed to make a panic button discretely into a watch you can bring anywhere you go in the office premises, business establishment or any workplace.</p>
              <p>Complete with its own command centre that function as a professional central monitoring system always on the lookout for any untoward incident, and admin portal where managing information is made easier and in order.</p>
              <p>
              Comes with a desktop app and phone app that works exactly like the watch. It also has a separate alert button that makes raising an alert even more easier especially for senior citizens and persons with disability.
              </p>
              <p>
              Sending emergency signals is now easier within the workspace. With the latest technology, calling for help during any type of emergency is at your fingertips.
              </p>
            </div>
          </div>
        </div>
      </div>
      {
        typeof window !== "undefined" && (
          <ModalVideo
            channel="vimeo"
            autoplay
            isOpen={isOpen}
            videoId="127203262"
            onClose={() => setOpen(false)}
          />
        )
      }
    </section>
  )
}

export default Services5