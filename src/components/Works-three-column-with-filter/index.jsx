/* eslint-disable @next/next/no-img-element */
import React from "react";
import Link from "next/link";
import initIsotope from "../../common/initIsotope";

const WorksThreeColumnWithFilter = ({ filterPosition }) => {
  const [pageLoaded, setPageLoaded] = React.useState(false);
  React.useEffect(() => {
    setPageLoaded(true);
    if (pageLoaded) {
      setTimeout(() => {
        initIsotope();
      }, 1000)
    }
  }, [pageLoaded]);
  return (
    <section className="portfolio section-padding pb-70">
      <div className="container">
        <div className="row">
          <div
            className={`filtering ${
              filterPosition === "center"
                ? "text-center"
                : filterPosition === "left"
                ? "text-left"
                : "text-right"
            } smplx col-12`}
          >
            <div className="filter">
              <span data-filter="*" className="active">
                All
              </span>
              <span data-filter=".directwireless">Direct Wireless</span>
              <span data-filter=".directconnect">Direct Connect</span>
              <span data-filter=".watcharmour">Watch Armour</span>
              <span data-filter=".watchguardian">Watch Guardian</span>
            </div>
          </div>

          <div className="gallery full-width">
            <div className="col-lg-4 col-md-6 items directconnect lg-mr">
              <div className="item-img wow fadeInUp" data-wow-delay=".4s">
                <Link href="/">
                  <a>
                    <img src="/img/portfolio/mas/DC04.png" alt="image" />
                  </a>
                </Link>
              </div>
              <div className="cont">
                <h6>Direct Connect</h6>
                <span>
                  <a href="#0">lorem</a>, <a href="#0">ipsum</a>
                </span>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 items directconnect">
              <div className="item-img wow fadeInUp" data-wow-delay=".4s">
                <Link href="/">
                  <a>
                    <img src="/img/portfolio/mas/dc02.png" alt="image" />
                  </a>
                </Link>
              </div>
              <div className="cont">
                <h6>Direct Connect</h6>
                <span>
                  <a href="#0">lorem</a>, <a href="#0">ipsum</a>
                </span>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 items directconnect lg-mr">
              <div className="item-img wow fadeInUp" data-wow-delay=".4s">
                <Link href="/">
                  <a>
                    <img src="/img/portfolio/mas/DC-1X.png" alt="image" />
                  </a>
                </Link>
              </div>
              <div className="cont">
                <h6>Direct Connect</h6>
                <span>
                  <a href="#0">Lorem</a>, <a href="#0">Ipsum</a>
                </span>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 items watcharmour">
              <div className="item-img wow fadeInUp" data-wow-delay=".4s">
                <Link href="/">
                  <a>
                    <img src="/img/portfolio/mas/wa01.png" alt="image" />
                  </a>
                </Link>
              </div>
              <div className="cont">
                <h6>Watch Armour</h6>
                <span>
                  <a href="#0">Lorem</a>, <a href="#0">Ipsum</a>
                </span>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 items watchguardian">
              <div className="item-img wow fadeInUp" data-wow-delay=".4s">
                <Link href="/">
                  <a>
                    <img src="/img/portfolio/mas/wg01.png" alt="image" />
                  </a>
                </Link>
              </div>
              <div className="cont">
                <h6>Watch Guardian</h6>
                <span>
                  <a href="#0">lorem</a>, <a href="#0">ipsum</a>
                </span>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 items directconnect">
              <div className="item-img wow fadeInUp" data-wow-delay=".4s">
                <Link href="/">
                  <a>
                    <img src="/img/portfolio/mas/dc01.png" alt="image" />
                  </a>
                </Link>
              </div>
              <div className="cont">
                <h6>Direct Connect</h6>
                <span>
                  <a href="#0">lorem</a>, <a href="#0">ipsum</a>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default WorksThreeColumnWithFilter;
