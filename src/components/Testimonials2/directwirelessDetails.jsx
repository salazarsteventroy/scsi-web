import { useState, useRef, useEffect } from 'react';
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination } from "swiper";
import removeSlashFromPagination from '../../common/removeSlashFromPagination';
import testimonialsData from '../../data/sections/directwirelessdata.json';

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

SwiperCore.use([Pagination, Navigation]);

const DirectWirelessDetails = () => {
  const [load, setLoad] = useState(true);
  const [swiperBackground, setSwiperBackground] = useState('');
  const paginationRef = useRef(null);
  const navigationPrevRef = useRef(null);
  const navigationNextRef = useRef(null);
  const swiperRef = useRef(null);

  useEffect(() => {
    setTimeout(() => {
      setLoad(false);
      removeSlashFromPagination();
    }, 0);
  }, []);

  useEffect(() => {
    if (!load && testimonialsData.length > 0) {
      setSwiperBackground(testimonialsData[0].image); // Set initial background image based on first testimonial
    }
  }, [load]);

  const handleSlideChange = () => {
    const currentSlide = swiperRef.current.swiper.realIndex;
    setSwiperBackground(testimonialsData[currentSlide].image);
  };

  return (
    <section className="testim-box">
      <div className="container"
        style={{
            backgroundColor:"#001a21",
            borderRadius:"50px",
            marginBottom:"50px"
        }}
      >
        <div className="row">
          <div className="col-lg-12">
            <div className="swiper-container bg-img" style={{ backgroundImage: `url('${swiperBackground}')` }} data-overlay-dark="1">
              {!load && (
                <Swiper
                  ref={swiperRef}
                  className="swiper-wrapper"
                  slidesPerView={1}
                  spaceBetween={0}
                  loop={true}
                  speed={800}
                  autoplay={{
                    delay: 2500,
                    disableOnInteraction: false,
                  }}
                  navigation={{
                    prevEl: navigationPrevRef.current,
                    nextEl: navigationNextRef.current,
                  }}
                  pagination={{
                    el: paginationRef.current,
                    clickable: true,
                    type: 'fraction'
                  }}
                  onBeforeInit={(swiper) => {
                    swiper.params.navigation.prevEl = navigationPrevRef.current;
                    swiper.params.navigation.nextEl = navigationNextRef.current;
                    swiper.params.pagination.el = paginationRef.current;
                  }}
                  onSwiper={(swiper) => {
                    setTimeout(() => {
                      swiper.params.navigation.prevEl = navigationPrevRef.current;
                      swiper.params.navigation.nextEl = navigationNextRef.current;

                      swiper.params.pagination.el = paginationRef.current;

                      swiper.navigation.destroy();
                      swiper.navigation.init();
                      swiper.navigation.update();

                      swiper.pagination.destroy();
                      swiper.pagination.init();
                      swiper.pagination.update();
                    });
                  }}
                  onSlideChange={handleSlideChange}
                >
                  {testimonialsData.map((testimonial) => (
                    <SwiperSlide key={testimonial.id}>
                      <div className="row">
                        <div className="col-lg-8 col-md-10">
                          <div className="item">
                            <h6>{testimonial.name}</h6>
                            
                            <p>{testimonial.comment1}</p>
                            <p>{testimonial.comment2}</p>
                            <p>{testimonial.comment3}</p>
                            <p>{testimonial.comment4}</p>
                            <p>{testimonial.comment5}</p>
                            <p>{testimonial.comment6}</p>
                            <p>{testimonial.comment7}</p>
                            <p>{testimonial.comment8}</p>
                          </div>
                        </div>
                      </div>
                    </SwiperSlide>
                  ))}
                </Swiper>
              )}

              <div className="controls">
                <div className="swiper-button-prev swiper-nav-ctrl prev-ctrl cursor-pointer" ref={navigationPrevRef}>
                  <i className="fas fa-chevron-left"></i>
                </div>
                <div className="swiper-button-next swiper-nav-ctrl next-ctrl cursor-pointer" ref={navigationNextRef}>
                  <i className="fas fa-chevron-right"></i>
                </div>
              </div>
              <div className="swiper-pagination custom-font" ref={paginationRef}></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default DirectWirelessDetails;
