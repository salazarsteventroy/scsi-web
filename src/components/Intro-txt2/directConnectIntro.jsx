import { useEffect } from 'react';
import addParlx from '../../common/addParlx';

const DirectConnectIntro = () => {
  useEffect(() => {
    setTimeout(() => addParlx(), 0);
  }, []);

  return (
    <header className="slider simpl fixed-slider bg-img valign" style={{ backgroundImage: "url(/img/slid/DcLogo01.png)" }} data-overlay-dark="6">
      <div className="container">
        <div className="row">
          <div className="col-lg-9">
            <div className="caption center mt-50">
              <h6>SCSI</h6>
              <h1>Direct Connect</h1>
              <p>Secure 4g Service.
                <br />
                With fixed IP&apos;s for your CCTV, Medical or other field devices
              </p>
              {/* <a href="#0" className="btn-curve btn-lit mt-40">
                <span>Get Started Now</span>
              </a> */}
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}

export default DirectConnectIntro;
