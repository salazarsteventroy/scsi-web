#https://thoughtrealm.medium.com/deploying-a-next-js-app-with-nginx-using-docker-ca6a5bbb902e
#stage - dependencies -> build
FROM node:20-alpine AS build

# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat

#canvas dependencies
RUN apk add --no-cache \
    make \
    build-base \
    python3 \
    g++ \
    cairo-dev \
    pango-dev \
    jpeg-dev 

WORKDIR /app

COPY package*.json ./

RUN npm install --legacy-peer-deps

COPY . .

RUN npm run build

FROM node:20-alpine AS production
RUN apk add --no-cache \
    cairo \
    pango \
    libjpeg 

WORKDIR /app

ENV NODE_ENV production
# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry during the build.
ENV NEXT_TELEMETRY_DISABLED 1

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing

COPY --from=build --link /app/public ./public
COPY --from=build --link --chown=1001:1001 /app/.next/standalone ./
COPY --from=build --link --chown=1001:1001 /app/.next/static ./.next/static

EXPOSE 3000

CMD ["node", "server.js"]